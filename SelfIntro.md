# 杨勇个人自我介绍

## 个人基本信息

姓名：杨勇

性别： 男

出生年份：1990

家乡：浙江.绍兴.上虞

身高：175 cm

教育背景：硕士浙江大学，本科长安大学

工作情况：程序员，刚从杭州的上家公司离职，2021年一月开始在上海的一家外企做软件开发工作，工作生活平衡



## 兴趣爱好

个人现在主要的兴趣爱好有三项，吉他，舞蹈和滑板。

### 吉他

吉他最早是大一的时候接触的，不过当时没有老师，自己学走了很多弯路，后来找到了[JustinGuitar](JustinGuitar.com)教学网站，系统地跟随学习，卓有成效，另外线下找了吉他老师学习电吉他Blues，老师可以给与反馈和纠正错误。

目前吉他的水平是中阶水平，可以做节奏吉他，以及简单的Solo。个人倾向的风格是Blues， Rock&Roll。等到自己技术提升到某一水平，有考虑找相投的乐友组乐队玩。

### 舞蹈

目前系统学习摩登舞三年有余，主要是华尔兹，狐步，快步和探戈。与摩登舞结缘是在学校的交谊舞课上，初想是认识女生，后来坚持下来就变成了爱好。

另外对Swing Dance很感兴趣，后续想学习了解，很喜欢Blues，Jazz的自由和律动。

## 滑板

滑板是2019年夏天开始的，感觉滑板少年和自由，很酷。目前是初学者水平，学会了基础滑行，Ollie和Shoveit.

## 音乐

个人喜欢的音乐类型主要是Rock&Roll，Blues

喜欢的音乐人或乐队有：

The Beatles, The Kinks， The Rolling Stone, The Who, The Velvet Underground

Nivarna, U2, Neil Young,  David Bowie, Blur, Oasis, Pink Floyd, Coldplay

Chuck Berry, Little Richard,  Eric Clapton, B.B King, Jimi Hendrix, SRV,

Queen, The Cranberries,  The Carpenters， DEPAPEPE, Lenka

国内喜欢的音乐人有周杰伦和窦唯。

## 电影/电视剧

喜欢的电影或电视剧有

**美丽心灵 A Beautiful Mind**            **(2001)**

**心灵捕手 Good Will Hunting**            **(1997)**

**死亡诗社 Dead Poets Society**            **(1989)**

**神秘博士 Dr. Who**

**硅谷 Silicon Valley**

...

## 动漫

动漫看的不多，但也看过一些，印象中比较好的作品有

**EVA 新世纪福音战士 新世紀エヴァンゲリオン**            **(1995)**

**虫师 蟲師**            **(2005)**

**进击的巨人**

**精灵宝可梦**

## 游戏

游戏也玩的不多，有时间愿意探索新游戏，最近玩的几个游戏

**Don't Starve 饥荒**

**Human： Fall Apart**

**Dirty Rally**

### 最近看的非专业的书

**European Intellectual History 从卢梭到尼采 Frank M.Turner**  

**剑桥中国明代史**

**与罗摩相会 Rendezvous with Rama by Arthur Charles Clarke**



## 个人性格

个人性格有点偏内向，感性驱动理性，有尝试和探索精神，有良好的沟通和交流能力，喜欢交流科技，人文，经济和艺术话题



## 对另一半的期望

美丽聪慧，温柔性感，互相尊重彼此作为独立的个体，有共同话题，可以一起探索广阔的世界，包括物质世界和精神文化世界，交流和分享彼此的经历和想法



## 王道时间

![孟买海边](mumbai-selfie.jpg)



![西溪湿地](xixi-standing.jpg)

![滑板](sb.jpg)